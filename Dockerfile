FROM node:14-alpine

RUN mkdir -p /app
RUN mkdir -p /frontend

COPY ./backend/package*.json /app/
COPY ./frontend/package*.json /frontend/

RUN cd /app && npm i --only=production
RUN cd /frontend && npm i

COPY ./frontend/src /frontend/src/
COPY ./frontend/public /frontend/public/
COPY ./frontend/*.js /frontend/

RUN cd /frontend && npm run build
RUN mv /frontend/dist /app/views

COPY ./backend/server.js /app/server.js

EXPOSE 3000
ENTRYPOINT ["node", "/app/server.js"]
