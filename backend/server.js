const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mysql = require("mysql2");
const path = __dirname + "/views/";
const server = express();
server.use(express.static(path));

server.use(cors({ origin: "http://localhost:8080" }));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

const db = mysql.createConnection({
  host: "mysql.ps4-2024-example.svc.cluster.local",
  user: "ps4user",
  password: "ksJOBt49kW98beZ088rV",
  database: "ps4_db",
});

db.connect((err) => {
  if (err) throw err;
  console.log("Connected to the database!");
});

server.get("/api/mydata", function (req, res) {
  db.query("SELECT * FROM mydata", (err, result) => {
    if (err) throw err;
    res.json(result);
  });
});

server.get("*", function (req, res) {
  res.sendFile(path + "index.html");
});

// set port, listen for requests
const PORT = process.env.PORT || 80;

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
